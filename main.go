package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/samber/lo"
	"gopkg.in/yaml.v3"
	"os"
	"regexp"
	"strings"
)

func main() {
	var template string
	flag.StringVar(&template, "template", "renovate.template.json", "template")

	var config string
	flag.StringVar(&config, "config", "renovate.yml", "config")

	if err := app(template, config); err != nil {
		panic(err)
	}
}

type Config struct {
	Regex RegexConfig `yaml:"regex"`
}

type RegexConfig struct {
	Files   []string             `yaml:"files"`
	Matches []RegexConfigMatches `yaml:"matches"`
}

type RegexConfigMatches struct {
	Datasource      string `yaml:"datasource"`
	DepName         string `yaml:"depName"`
	AllowedVersions string `yaml:"allowedVersions"`
	Name            string `yaml:"name"`
	CurrentValue    string `yaml:"currentValue"`
}

type Renovate struct {
	Extends         []string               `json:"extends"`
	EnabledManagers []string               `json:"enabledManagers"`
	Reviewers       []string               `json:"reviewers"`
	RecreateClosed  bool                   `json:"recreateClosed"`
	RegexManagers   []RenovateRegexManager `json:"regexManagers"`
}

type RenovateRegexManager struct {
	FileMatch               []string `json:"fileMatch"`
	MatchStrings            []string `json:"matchStrings"`
	AllowedVersionsTemplate string   `json:"allowedVersionsTemplate"`
}

func app(templateFlag, configFlag string) error {
	templateBytes, err := os.ReadFile(templateFlag)
	if err != nil {
		return err
	}

	configBytes, err := os.ReadFile(configFlag)
	if err != nil {
		return err
	}

	var template Renovate
	if err := json.Unmarshal(templateBytes, &template); err != nil {
		return err
	}

	var config Config
	if err := yaml.Unmarshal(configBytes, &config); err != nil {
		return err
	}

	manager := template.RegexManagers[0]
	manager.FileMatch = lo.Map(config.Regex.Files, func(file string, _ int) string {
		return regexp.QuoteMeta(file)
	})
	manager.MatchStrings = lo.Map(config.Regex.Matches, func(match RegexConfigMatches, _ int) string {
		return constructMatchString(match)
	})

	template.RegexManagers[0] = manager

	fmt.Println(fmt.Sprintf("%+v", template))
	fmt.Println(fmt.Sprintf("%+v", config))

	fmt.Println(regexp.QuoteMeta(config.Regex.Files[0]))

	finalRenovate, _ := json.MarshalIndent(template, " ", "  ")
	return os.WriteFile("renovate.json", finalRenovate, 0644)
}

func constructMatchString(match RegexConfigMatches) string {
	var builder strings.Builder
	builder.WriteString("# renovate: ")
	if match.Datasource != "" {
		builder.WriteString(constructExactGroupMatch("datasource", match.Datasource))
	}

	if match.DepName != "" {
		builder.WriteString(constructExactGroupMatch("depName", match.DepName))
	}

	if match.AllowedVersions != "" {
		builder.WriteString(constructExactGroupMatch("allowedVersions", match.AllowedVersions))
	}

	if match.Name != "" {
		builder.WriteString(fmt.Sprintf(`\s%s`, match.Name))
	}

	if match.CurrentValue == "" {
		match.CurrentValue = `[\w+\.\-]*`
	}

	builder.WriteString(constructExactGroupMatch("currentValue", match.CurrentValue))

	return builder.String()
}

func constructExactGroupMatch(groupName, valueMatch string) string {
	return fmt.Sprintf("(?<%s>%s)", groupName, valueMatch)
}
